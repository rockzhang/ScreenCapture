//--------------------------------------------------------------------
// FileName: ImageConverter.h
// Author: zhuyf
// Version: 0.1
// Date: 2013.5.10
// Description: 使用 GDI+ 转换图片格式
//
// History:
// 1.Date:
//   Author:
//   Modification:
//
//--------------------------------------------------------------------
#pragma once

#include <GdiPlus.h>

class CImageConverter
{
public:
	//
	//【功能】：初始化 图片转换器
	//
	void Init();

	//
	//【功能】：释放 图片转换器
	//
	void Deinit();

	//
	//【功能】：将BMP格式的图片转换为PNG格式
	//【参数】：szBmp：[in]BMP图片的完整路径名
	//	    szPng：[in]PNG图片的完整路径名
	//
	bool BmpToPng(LPCWSTR szBmp, LPCWSTR szPng);

	//
	//【功能】：将PNG格式的图片转换为BMP格式
	//【参数】：szPng：[in]PNG图片的完整路径名
	//	    szBmp：[in]BMP图片的完整路径名
	//
	bool PngToBmp(LPCWSTR szPng, LPCWSTR szBmp);

	//
	//【功能】：将BMP格式的图片转换为JPG格式
	//【参数】：szBmp：[in]BMP图片的完整路径名
	//	    szJpg：[in]JPG图片的完整路径名
	//
	bool BmpToJpg(LPCWSTR szBmp, LPCWSTR szJpg);

	//
	//【功能】：将JPG格式的图片转换为BMP格式
	//【参数】：szJpg：[in]JPG图片的完整路径名
	//	    szBmp：[in]BMP图片的完整路径名
	//
	bool JpgToBmp(LPCWSTR szJpg, LPCWSTR szBmp);

protected:
	bool GetEncoderClsid(TCHAR *pFormat, CLSID *pClsid);

	Gdiplus::GdiplusStartupInput input_;
	ULONG_PTR token_;

public:
	CImageConverter(void);
	~CImageConverter(void);
};