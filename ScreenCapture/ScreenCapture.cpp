// ScreenCapture.cpp : Defines the entry point for the console application.
//



#include <Windows.h>
#include <math.h>

#include <windows.h>
#include <gdiplus.h>
#include "stdafx.h"
using namespace Gdiplus;

#pragma comment(lib,"gdiplus")

void ScreenSnap(HBITMAP hBitmap,char *bmpPath,HDC dc);
void Stretch(const char* srcFile, const char* desFile, int desW, int desH);

int GetEncoderClsid(const WCHAR* format, CLSID* pClsid)
{
	UINT  num = 0;          // number of image encoders
	UINT  size = 0;         // size of the image encoder array in bytes

	ImageCodecInfo* pImageCodecInfo = NULL;

	//2.获取GDI+支持的图像格式编码器种类数以及ImageCodecInfo数组的存放大小
	GetImageEncodersSize(&num, &size);
	if(size == 0)
		return -1;  // Failure

	//3.为ImageCodecInfo数组分配足额空间
	pImageCodecInfo = (ImageCodecInfo*)(malloc(size));
	if(pImageCodecInfo == NULL)
		return -1;  // Failure

	//4.获取所有的图像编码器信息
	GetImageEncoders(num, size, pImageCodecInfo);

	//5.查找符合的图像编码器的Clsid
	for(UINT j = 0; j < num; ++j)
	{
		if( wcscmp(pImageCodecInfo[j].MimeType, format) == 0 )
		{
			*pClsid = pImageCodecInfo[j].Clsid;
			free(pImageCodecInfo);
			return j;  // Success
		}    
	}

	//6.释放步骤3分配的内存
	free(pImageCodecInfo);
	return -1;  // Failure
}


int _tmain(int argc, _TCHAR* argv[])
{ 
	if(argc < 2)
	{
		printf("Usage: ScreenCapture.exe File_Store_Path\nFor example: ScreenCapture.exe d:\\\\zhang.bmp");
		return -1;
	}
	HWND DeskWnd=::GetDesktopWindow();//获取桌面窗口句柄
	RECT DeskRC;
	::GetClientRect(DeskWnd,&DeskRC);//获取窗口大小
	HDC DeskDC=GetDC(DeskWnd);//获取窗口DC
	HBITMAP DeskBmp=::CreateCompatibleBitmap(DeskDC,DeskRC.right,DeskRC.bottom);//兼容位图
	HDC memDC=::CreateCompatibleDC(DeskDC);//兼容DC
	SelectObject(memDC,DeskBmp);//把兼容位图选入兼容DC中
	BitBlt(memDC,0,0,DeskRC.right,DeskRC.bottom,DeskDC,0,0,SRCCOPY);//拷贝DC
	ScreenSnap(DeskBmp,argv[1],DeskDC);
	printf("Capture success\n");
	return 0;
}

void ConvertPicture(char * bmpPath, char*bmpPicture);

void ScreenSnap(HBITMAP hBitmap, char *bmpPath,HDC dc)
{
	BITMAP bmInfo;
	DWORD bmDataSize;
	char *bmData;//位图数据
	GetObject(hBitmap,sizeof(BITMAP),&bmInfo);//根据位图句柄，获取位图信息
	bmDataSize=bmInfo.bmWidthBytes*bmInfo.bmHeight;//计算位图数据大小
	bmData=new char[bmDataSize];//分配数据
	BITMAPFILEHEADER bfh;//位图文件头
	bfh.bfType=0x4d42;
	bfh.bfSize=bmDataSize+54;
	bfh.bfReserved1=0;
	bfh.bfReserved2=0;
	bfh.bfOffBits=54;
	BITMAPINFOHEADER bih;//位图信息头
	bih.biSize=40;
	bih.biWidth=bmInfo.bmWidth;
	bih.biHeight=bmInfo.bmHeight;
	bih.biPlanes=1;
	bih.biBitCount=24;
	bih.biCompression=BI_RGB;
	bih.biSizeImage=bmDataSize;
	bih.biXPelsPerMeter=0;
	bih.biYPelsPerMeter=0;
	bih.biClrUsed=0;
	bih.biClrImportant=0;

	::GetDIBits(dc,hBitmap,0,bmInfo.bmHeight,bmData,(BITMAPINFO *)&bih,DIB_RGB_COLORS);//获取位图数据部分
	HANDLE hFile=CreateFile(bmpPath,GENERIC_WRITE,0,NULL,CREATE_ALWAYS,
		FILE_ATTRIBUTE_NORMAL,0);//创建文件
	if(hFile == INVALID_HANDLE_VALUE)
	{
		printf("Create file %s failed, make sure you have permission to create file\n", bmpPath);
		return ;
	}
	DWORD dwSize;
	WriteFile(hFile,(void *)&bfh,sizeof(BITMAPFILEHEADER),&dwSize,0);//写入位图文件头
	WriteFile(hFile,(void *)&bih,sizeof(BITMAPINFOHEADER),&dwSize,0);//写入位图信息头
	WriteFile(hFile,(void *)bmData,bmDataSize,&dwSize,0);//写入位图数据
	::CloseHandle(hFile);//关闭文件句柄
	
	//Stretch(fileTemp, bmpPath, bmInfo.bmWidthBytes/8, bmInfo.bmHeight/2);

#if 1
	char fileTemp[256] = {0};
	strcpy_s(fileTemp, bmpPath);
	strcat_s(fileTemp, ".png");
#endif
	ConvertPicture(bmpPath, fileTemp);
	DeleteFile(bmpPath);
}

void ConvertPicture(char * bmpPicture, char * pngPicture)
{

	wchar_t bmpWidePicture[256] = {0};
	wchar_t savePngPicture[256] = {0};

	MultiByteToWideChar(CP_ACP, 0, bmpPicture, strlen(bmpPicture), bmpWidePicture, 256);
	MultiByteToWideChar(CP_ACP, 0, pngPicture, strlen(pngPicture), savePngPicture, 256);
	
	GdiplusStartupInput gdiplusStartupInput;
	ULONG_PTR gdiplusToken;

	//1.初始化GDI+，以便后续的GDI+函数可以成功调用
	GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);

	CLSID   encoderClsid;
	Status  stat;

	//7.创建Image对象并加载图片
	Image*   image = new Image(bmpWidePicture);

	// Get the CLSID of the PNG encoder.
	GetEncoderClsid(L"image/png", &encoderClsid);

	//8.调用Image.Save方法进行图片格式转换，并把步骤3)得到的图像编码器Clsid传递给它
	stat = image->Save(savePngPicture, &encoderClsid, NULL);

	if(stat == Ok)
		printf("picture was saved successfully\n");
	else
		printf("Failure: stat = %d/n", stat); 

	//9.释放Image对象
	delete image;
	//10.清理所有GDI+资源
	GdiplusShutdown(gdiplusToken);
}

const int CUBIC_SIZE = 3;
int getSincVec(float *pfVec, float fPos, float fScale)
{
	int nPos = (int)fPos;
	fPos -= (float)nPos;
	memset(pfVec, 0, sizeof(float) * CUBIC_SIZE);
	float fSum = 0.f;
	for (int i=0; i<CUBIC_SIZE; i++)
	{
		float fTmp = (i - 1.f + fPos) * 3.14159265358979323846f;
		if (fTmp > 0.00001 || fTmp < -0.00001)
		{
			pfVec[i] = sinf(fTmp * fScale) / fTmp;
		}
		else
		{
			pfVec[i] = 1.f;
		}
		fSum += pfVec[i];
	}
	fSum = 1.f / fSum;
	for (int i=0; i<CUBIC_SIZE; i++)
	{
		pfVec[i] *= fSum;
	}
	return nPos;
}

void calcSincMat(float *pfMat, float *pfVecY, float *pfVecX)
{
	for (int i=0; i<CUBIC_SIZE; i++)
	{
		for (int j=0; j<CUBIC_SIZE; j++)
		{
			pfMat[i * CUBIC_SIZE + j] = pfVecY[i] * pfVecX[j];
		}
	}
}

void Stretch(const char* srcFile, const char* desFile, int desW, int desH)
{
	BITMAPFILEHEADER bmfHeader;
	BITMAPINFOHEADER bmiHeader;

	FILE *pFile;
	if ((pFile = fopen(srcFile,"rb")) == NULL)
	{
		printf("open bmp file error.");
		exit(-1);
	}
	//读取文件和Bitmap头信息
	fseek(pFile,0,SEEK_SET);
	fread(&bmfHeader,sizeof(BITMAPFILEHEADER),1,pFile);
	fread(&bmiHeader,sizeof(BITMAPINFOHEADER),1,pFile);
	//先不支持小于16位的位图
	int bitCount = bmiHeader.biBitCount;
	if (bitCount < 16)
	{ 
		exit(-1);
	}
	int srcW = bmiHeader.biWidth;
	int srcH = bmiHeader.biHeight;

	int lineSize = bitCount * srcW / 8;
	//偏移量，windows系统要求每个扫描行按四字节对齐
	int alignBytes = ((bmiHeader.biWidth * bitCount + 31) & ~31) / 8L
		- bmiHeader.biWidth * bitCount / 8L;
	//原图像缓存
	int srcBufSize = lineSize * srcH;
	BYTE* srcBuf = new BYTE[srcBufSize];
	int i,j;
	//读取文件中数据
	for (i = 0; i < srcH; i++)
	{ 
		fread(&srcBuf[lineSize * i],lineSize,1,pFile);
		fseek(pFile,alignBytes,SEEK_CUR);
	}

	//目标图像缓存
	int desBufSize = ((desW * bitCount + 31) / 32) * 4 * desH;
	int desLineSize = ((desW * bitCount + 31) / 32) * 4; 
	BYTE *desBuf = new BYTE[desBufSize];
	double rateH = (double)srcH / desH;
	double rateW = (double)srcW / desW;

	float pfVecX[CUBIC_SIZE], pfVecY[CUBIC_SIZE], pfMat[CUBIC_SIZE][CUBIC_SIZE];
	int nPixelBytes = bitCount / 8;
	for (i=1; i<desH-1; i++)
	{
		int nSrcY = getSincVec(pfVecY, rateH * i, 1.f / rateH);
		for (j = 1; j < desW-1; j++)
		{
			int nSrcX = getSincVec(pfVecX, rateW * j, 1.f / rateW);
			calcSincMat(pfMat[0], pfVecY, pfVecX);
			BYTE *pbyTmpDes = desBuf + i * desLineSize + j * nPixelBytes;
			BYTE *pbyTmpSrc = srcBuf + nSrcY * lineSize + nSrcX * nPixelBytes;
			for (int k = 0; k < 3; k++)
			{
				float fTmp = pbyTmpSrc[0] * pfMat[1][1];
				fTmp += pbyTmpSrc[-lineSize] * pfMat[0][1];
				fTmp += pbyTmpSrc[lineSize] * pfMat[2][1];
				fTmp += pbyTmpSrc[-nPixelBytes] * pfMat[1][0];
				fTmp += pbyTmpSrc[-nPixelBytes-lineSize] * pfMat[0][0];
				fTmp += pbyTmpSrc[-nPixelBytes+lineSize] * pfMat[2][0];
				fTmp += pbyTmpSrc[nPixelBytes] * pfMat[1][2];
				fTmp += pbyTmpSrc[nPixelBytes-lineSize] * pfMat[0][2];
				fTmp += pbyTmpSrc[nPixelBytes+lineSize] * pfMat[2][2];
				if (fTmp < 0.f) fTmp = 0.f;
				if (fTmp > 255.f) fTmp = 255.f;
				*pbyTmpDes++ = (BYTE)fTmp;
				pbyTmpSrc++;
			} 
		}
	}


	//创建目标文件
	HFILE hfile = _lcreat(desFile,0); 
	//文件头信息
	BITMAPFILEHEADER nbmfHeader; 
	nbmfHeader.bfType = 0x4D42;
	nbmfHeader.bfSize = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER)
		+ desW * desH * bitCount / 8;
	nbmfHeader.bfReserved1 = 0;
	nbmfHeader.bfReserved2 = 0;
	nbmfHeader.bfOffBits = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);
	//Bitmap头信息
	BITMAPINFOHEADER bmi; 
	bmi.biSize=sizeof(BITMAPINFOHEADER); 
	bmi.biWidth=desW; 
	bmi.biHeight=desH; 
	bmi.biPlanes=1; 
	bmi.biBitCount=bitCount; 
	bmi.biCompression=BI_RGB; 
	bmi.biSizeImage=0; 
	bmi.biXPelsPerMeter=0; 
	bmi.biYPelsPerMeter=0; 
	bmi.biClrUsed=0; 
	bmi.biClrImportant=0; 

	//写入文件头信息
	_lwrite(hfile,(LPCSTR)&nbmfHeader,sizeof(BITMAPFILEHEADER));
	//写入Bitmap头信息
	_lwrite(hfile,(LPCSTR)&bmi,sizeof(BITMAPINFOHEADER));
	//写入图像数据
	_lwrite(hfile,(LPCSTR)desBuf,desBufSize);
	_lclose(hfile);
}
